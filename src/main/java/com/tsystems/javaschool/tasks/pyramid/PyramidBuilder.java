package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder  {
    
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
        // TODO : Implement your solution here
        try
        {
           if(inputNumbers.contains(null))
           {
             throw new CannotBuildPyramidException();
           }
           Collections.sort(inputNumbers);
           Collections.reverse(inputNumbers);
        }
        catch(OutOfMemoryError e)
        {
            throw new CannotBuildPyramidException();
        }
        
        int n = (int)findStringNumber(inputNumbers.size()); //число строк
        int m =(int)findColumnNumber(n); //число столбцов
        int k=0; //Переменная для определения номера элемента в последовательности чисел
        int t=0; //Вспомогательная переменная для формирования пирамиды
        
        int[][] mas = new int[n][m];
        for(int i=n-1;i>=0;i--)
        {
            for(int j=m-1-t;j>=0+t; j-=2)
            {
                if(k!=inputNumbers.size())
                {
                    mas[i][j]=inputNumbers.get(k);
                    k++;
                }
            }
            t++;
        }
        return mas;
    }
    
    /**
    * Пирамида строится, если количество элементов в списке является треугольным числом. 
    * Если число треугольное, то его номер в последовательности треугольных чисел равен количеству строк в пирамиде. 
    * Формула для нахождения n-го треуголного числа T=(n*(n+1))/2;
    */ 
    
    public double findStringNumber(int a)
    {
        double n=0; //число строк в пирамиде
        int D=0; //Дискриминант
        D=1+8*a; //Вычисляем дискриминант
        n=(Math.sqrt(D)-1)/2;
        if((n>0)&&((n-(int)n)==0))
        {
            return n;
        }
        else
        {
            throw new CannotBuildPyramidException();
        }
    }
    
    /**
     *  Ширина основания пирамиды находится по формуле арифметической прогрессии a[n]=a[1]+(n-1)*d;
     *  В нашем случае разность прогрессии равна d=2. a[1]=1 т.к вершина это один элемент.
     *  n равен количеству строк в пирамиде
    */
    
    public double findColumnNumber(int a)
    {
        double n=0; //ширина основания пирамиды
        
        n=1+(a-1)*2;
        
        return n;
    }
    
    
    
}
