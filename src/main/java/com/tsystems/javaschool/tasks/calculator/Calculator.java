package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;



public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        
        Stack<String> ops = new Stack<>(); //Стэк операторов
        Stack<String> vals = new Stack<>();//Стэк с числами
        
        String str = statement;
        String chis="";
        char[] mas = str.toCharArray();
        for(Character ch:mas)
        {
            String s=""+ch;
            if(s.equals("("))
            {
                if(!chis.equals(""))
                {
                vals.push(chis);
                chis="";
                }
            }
            else if(s.equals("+"))
            {
                if(!chis.equals(""))
                {
                vals.push(chis);
                chis="";
                }
                ops.push(s);
            }
            else if(s.equals("-"))
            {
                if(!chis.equals(""))
                {
                vals.push(chis);
                chis="";
                }
                ops.push(s);
            }
            else if(s.equals("*"))
            {
                if(!chis.equals(""))
                {
                vals.push(chis);
                chis="";
                }
                ops.push(s);
            }
            else if(s.equals("/"))
            {
                if(!chis.equals(""))
                {
                vals.push(chis);
                chis="";
                }
                ops.push(s);
            }
            else if(s.equals(")"))
            {
                if(!chis.equals(""))
                {
                vals.push(chis);
                chis="";
                }
                operate(ops,vals);
            }
            else
            {
                chis=chis+s;
            }
        }
        
        if(!chis.equals(""))
        {
            vals.push(chis);
        }
        
        while(!ops.empty())
        {
            operate(ops,vals);
        }
                        
        return rounding(vals.pop()); 
       
    }
    
     public void operate(Stack<String> s1, Stack<String> s2) //ops - s1   vals - s2
    {
        String op= s1.pop();
                double v=Double.parseDouble(s2.pop());
                if(op.equals("+"))
                {
                    v=Double.parseDouble(s2.pop())+v;
                }
                else if(op.equals("-"))
                {
                    v=Double.parseDouble(s2.pop())-v;
                }
                else if(op.equals("*"))
                {
                    v=Double.parseDouble(s2.pop())*v;
                }
                else if(op.equals("/"))
                {
                    v=Double.parseDouble(s2.pop())/v;
                }
                s2.push(Double.toString(v));
    }
    
     public String rounding(String val)
    {
        double d = Double.parseDouble(val);
        d=d*10000;
        int i = (int) Math.round(d);
        if((double)i % 10000==0)
        {
            return ""+i / 10000;
        }
        else
        {
            return ""+(double)i / 10000;
        }
    }

}
