/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Eugenie
 */
package com.tsystems.javaschool.tasks.subsequence;

import java.util.LinkedList;
import java.util.List;

public class Subsequence 
{
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        int i=0; //вспомогательная переменная
                 //количесво элементов из последовательности у совпавших с элементами и их порядком в последовательности х 
                 
        if((x==null)||(y==null))
        {
            throw new IllegalArgumentException();
        }         
        else         
        { 
            for(Object o:y)
            {
                if((i<x.size())&&(x.get(i).equals(o)))
                {
                    i++;
                }
            }
        }
        return x.size()==i;
    }
}
